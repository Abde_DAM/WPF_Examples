﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Speech.Synthesis;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Bingo
{
    /// <summary>
    /// Lógica de interacción para Joc.xaml
    /// </summary>
    public partial class Joc : UserControl
    {
        private List<int> elements, eliminats;
        private int numMinim=0, numMaxim=100, total=101, files = 0, columnes;
        private Button[,] matriu;
        private double frequencia;
        private bool pausa = false;
        int numBoto;
        private string pronuncia, aparicio, generacio;
        private int repeticions, tmpRepeticions=0;
        private MediaPlayer mediaPlayer = new MediaPlayer();
        private Color seleccionat, noSeleccionat;
        private DispatcherTimer dispatcherTimer;
        private double[] times = new double[] { 1.35, 2.54, 3.75, 4.9, 6.05, 7.29, 8.43, 9.72, 11.09, 12.16, 13.37, 14.5, 15.74, 16.92, 18.1, 19.24, 20.4, 21.59, 22.86, 24.01, 25.32, 26.52, 27.84, 29.12, 30.45, 31.83, 33.2, 34.66, 36.05, 37.32, 38.74, 39.81, 41.06, 42.28, 43.49, 44.8, 46.17, 47.51, 48.79, 50.03, 51.39, 52.45, 53.8, 55.05, 56.34, 57.65, 59.09, 60.43, 61.78, 63.03, 64.45, 65.49, 66.79, 68.04, 69.34, 70.7, 72.12, 73.47, 74.84, 76.2, 77.66, 79.14, 80.55, 82.18, 83.59, 85.58, 86.97, 88.43, 89.85, 91.22, 92.73, 94.01, 95.43, 96.81, 98.25, 99.63, 101.14, 102.54, 104.08, 105.54, 107.11, 108.15, 109.48, 110.75, 112.11, 113.49, 114.91, 116.34, 117.7, 119.08, 120.35, 121.57, 122.98, 124.31, 125.73, 127.13, 128.49, 129.9, 131.38, 132.72, 134.16, 135.28, 136.66, 137.42, 138.45, 139.75, 141.02, 143.09, 144.19, 145.22, 146.44 };
        SpeechSynthesizer speech;
        public Joc()
        {
            InitializeComponent();
            ObtenirDades();
            CrearButtons();
            InicialitzarMediaPlayer();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            btnNext.IsEnabled = aparicio.Equals(Opcions.MANUAL) && generacio.Equals(Opcions.ALEATORI) && elements.Count != 0;
            btnPlay.IsEnabled = aparicio.Equals(Opcions.AUTOMATIC);
            btnPause.IsEnabled = aparicio.Equals(Opcions.AUTOMATIC);
          //  btnStop.IsEnabled = aparicio.Equals(Opcions.AUTOMATIC);
            eliminats = new List<int>();
            speech = new SpeechSynthesizer();

        }


        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (tmpRepeticions <= repeticions)
            {

                tmpRepeticions++;
            }
            else
            {
                dispatcherTimer.Stop();
                tmpRepeticions = 0;
            }
            
        }

        private void InicialitzarMediaPlayer()
        {
            mediaPlayer.Open(new Uri(  @"  ../../src/numbers.m4a", UriKind.Relative));
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            Principal princ = new Principal();
            ((MainWindow)parentWindow).TransContent = princ;
            mediaPlayer.Stop();
            speech.SpeakAsyncCancelAll();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            int aleatori, i = 0, compt = elements.Count;
            if (aparicio.Equals(Opcions.MANUAL) && generacio.Equals(Opcions.ALEATORI)&&elements.Count!=0)
            {
                if (elements.Count > 0)
                {

                    aleatori = r.Next(1, elements.Count);
                    tbNumero.Text = aleatori.ToString();
                    i = 0;
                    PintaVermell(elements[aleatori]);
                    if (pronuncia == Opcions.GRABADA)
                    {
                        while (i <= repeticions)
                        {
                            mediaPlayer.Position = TimeSpan.FromSeconds(times[elements[aleatori]]);
                            mediaPlayer.Play();
                            Espera(1);
                            mediaPlayer.Pause();
                            Espera(frequencia);
                            i++;
                        }
                    }
                    else
                    {
                        while (i <= repeticions)
                        {
                            speech.Volume = 100;  // 0...100
                            speech.Rate = 0;
                            speech.SpeakAsync(aleatori.ToString());
                            Espera(frequencia + 1);
                            i++;
                        }
                    }
                    eliminats.Add(elements[aleatori]);
                    elements.RemoveAt(aleatori);
                }
            }
            
        }

        private void btnObtinguts_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            foreach (var item in eliminats)
            {
                message.Append(item.ToString());
                message.Append(" ");
            }
            Mostar(message);

        }

        private async void Mostar(StringBuilder message)
        {
            MetroWindow metroWindow = (Application.Current.MainWindow as MetroWindow);
            await metroWindow.ShowMessageAsync("Números", message.ToString());
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            int comptador = 0, i=0, j=0;
            while ( j < matriu.GetLength(1)&&comptador<total)
            {
                i = 0;
                while(i < matriu.GetLength(0) && comptador < total)
                {
                    matriu[i,j].Background = new SolidColorBrush(noSeleccionat);
                    comptador++;
                    i++;
                }
                j++;
            }
            mediaPlayer.Stop();
            elements = new List<int>();
            for (int k = numMinim; k < numMaxim; k++)
            {
                elements.Add(k);
            }
            eliminats = new List<int>();
            pausa = true;
            
            speech.SpeakAsyncCancelAll();


        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            pausa = false;
            if (aparicio.Equals(Opcions.AUTOMATIC))
                RandomButton();

        }
        

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            speech.SpeakAsyncCancelAll();
            pausa = true;
        }
        private void Espera(double segons)
        {
            var frame = new DispatcherFrame();
            new Thread((ThreadStart)(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(segons));
                frame.Continue = false;
            })).Start();
            Dispatcher.PushFrame(frame);
        }
        private void CrearButtons()
        {
            int comptador = numMinim;
            columnes = (int)Math.Truncate(Math.Sqrt(total));
            files = (int)Math.Ceiling(total / (double)columnes);
            elements = new List<int>();
            for (int i = numMinim; i <= numMaxim; i++)
            {
                elements.Add(i);
            }
            for (int i = 0; i < columnes; i++)
            {
                gdNumeros.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < files; i++)
            {
                gdNumeros.RowDefinitions.Add(new RowDefinition());

            }
            matriu = new Button[columnes, files];

            for (int j = 0; j < files; j++)
            {
                // TODO Canviar a while aquesta aberració de for
                for (int i = 0; i < columnes && comptador <= numMaxim; i++)
                {
                    matriu[i,j] = new Button();
                    matriu[i, j].Content = comptador;
                    matriu[i, j].Click += Button_Click;
                    matriu[i, j].Style = (Style)FindResource("MetroCircleButtonStyle");
                    matriu[i, j].Background = new SolidColorBrush(noSeleccionat);
                    gdNumeros.Children.Add(matriu[i, j]);
                    Grid.SetColumn(matriu[i, j], i);
                    Grid.SetRow(matriu[i, j], j);
                    matriu[i, j].Width =gdNumeros.RowDefinitions[0].Height.Value;
                    comptador++;

                    Binding myBinding = new Binding();
                    myBinding.Source = matriu[i, j];
                    myBinding.Path = new PropertyPath("ActualHeight");
                    myBinding.Mode = BindingMode.OneWay;
                    myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    BindingOperations.SetBinding(matriu[i, j], Button.WidthProperty, myBinding);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(((SolidColorBrush)((Button)sender).Background).Color==new SolidColorBrush(noSeleccionat).Color)
            if (aparicio != Opcions.AUTOMATIC&&generacio!=Opcions.ALEATORI)
            {
                numBoto = int.Parse(((Button)sender).Content.ToString());
                ((Button)sender).Background = new SolidColorBrush(seleccionat);
                    eliminats.Add(numBoto);
                tbNumero.Text = numBoto.ToString();
                if (pronuncia == Opcions.GRABADA)
                {
                    for (int i = 0; i <= repeticions; i++)
                    {
                        mediaPlayer.Position = TimeSpan.FromSeconds(times[numBoto]);
                        mediaPlayer.Play();
                        Espera(1);
                        mediaPlayer.Pause();
                        Espera(frequencia);
                    }
                }
                else
                    {
                        for (int i = 0; i <= repeticions; i++)
                        {
                            speech.Volume = 100;  // 0...100
                            speech.Rate = 0;
                            speech.SpeakAsync(numBoto.ToString());
                            Espera(frequencia);
                        }
                    }
                    
            }
        }

        private void ObtenirDades()
        {
            if (File.Exists(Opcions.ARXIUSPREFS))
            {
                PreferenciesUsuari up;
                XmlSerializer mySerializer = new XmlSerializer(typeof(PreferenciesUsuari));
                FileStream myFileStream = new FileStream(Opcions.ARXIUSPREFS, FileMode.Open);

                up = (PreferenciesUsuari)mySerializer.Deserialize(myFileStream);
                total = up.Maxim - up.Minim + 1;
                numMinim = up.Minim;
                numMaxim = up.Maxim;
                frequencia = up.Frequencia;
                pronuncia = up.Pronuncia;
                aparicio = up.Aparicio;
                generacio = up.Generacio;
                repeticions = up.Repeticions;
                if (up.ColorSortit != null)
                    seleccionat = (Color)ColorConverter.ConvertFromString(up.ColorSortit);

                if (up.ColorNoSortit != null)
                    noSeleccionat = (Color)ColorConverter.ConvertFromString(up.ColorNoSortit);

                myFileStream.Close();
            }
            else
            {
                total = 100;
                numMaxim = 100;
                numMinim = 1;
                frequencia = 2;
                pronuncia = Opcions.TEXTTOSPEECH;
                aparicio = Opcions.MANUAL;
                generacio = Opcions.ALEATORI;
                repeticions = 2;
                seleccionat = (Color)ColorConverter.ConvertFromString("#BB0000");
                noSeleccionat = (Color)ColorConverter.ConvertFromString("#FF008000");
            }
        }
        private void RandomButton()
        {
            Random r = new Random();
            int aleatori,  i=0, compt=elements.Count;
            for (int k = 0; k < compt&&!pausa; k++)
            {
                if (elements.Count > 0)
                {
                    aleatori = r.Next(0, elements.Count);
                    tbNumero.Text = elements[aleatori].ToString();
                    PintaVermell(elements[aleatori]);

                    i = 0;
                    if (pronuncia == Opcions.GRABADA)
                    {
                        while (i <= repeticions)
                        {
                            mediaPlayer.Position = TimeSpan.FromSeconds(times[elements[aleatori]]);
                            mediaPlayer.Play();
                            Espera(1);
                            mediaPlayer.Pause();
                            Espera(frequencia);
                            i++;
                        }
                    }
                    else
                    {
                        while (i <= repeticions)
                        {
                            speech.Volume = 100;  // 0...100
                            speech.Rate = 0;
                            speech.SpeakAsync(elements[aleatori].ToString());
                            Espera(frequencia + 1);
                            i++;
                        }
                    }
                    eliminats.Add(elements[aleatori]);
                    elements.RemoveAt(aleatori);
                    
                }
            }
          
        }
        private void PintaVermell(int numero)
        {
            int i = 0, j = 0, comptador=0;
            bool trobat = false;
            while (j < files&&!trobat&&comptador<total)
            {
                i = 0;
                while (i<columnes&&!trobat && comptador < total)
                {
                    if (matriu[i, j].Content.ToString() == numero.ToString())
                    {
                        trobat = true;
                        matriu[i, j].Background = new SolidColorBrush(seleccionat);
                    }
                    i++;
                    comptador++;
                }
                j++;
            }
        }

    }
}
