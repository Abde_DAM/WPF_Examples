﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Bingo
{
    /// <summary>
    /// Lógica de interacción para Preferencies.xaml
    /// </summary>
    public partial class Preferencies : UserControl
    {
        public Preferencies()
        {
            InitializeComponent();
            LoadPrefs();
        }

        private void LoadPrefs()
        {
            if (File.Exists(Opcions.ARXIUSPREFS))
            {
                try {
                    PreferenciesUsuari up;

                    XmlSerializer mySerializer = new XmlSerializer(typeof(PreferenciesUsuari));
                    FileStream myFileStream = new FileStream("prefs.xml", FileMode.Open);

                    up = (PreferenciesUsuari)mySerializer.Deserialize(myFileStream);
                    nudMaxim.Value = up.Maxim;
                    nudMinim.Value = up.Minim;
                    tbFreq.Value =up.Frequencia;
                    nudRepeticions.Value = up.Repeticions;
                    if (up.Aparicio != null)
                        rbtAutomatic.IsChecked = up.Aparicio.Equals(Opcions.AUTOMATIC);
                    if (up.Pronuncia != null)
                    {
                        rbtVeuGrabada.IsChecked = up.Pronuncia.Equals(Opcions.GRABADA);
                    }
                    if (up.Generacio!=null)
                    {
                        rbtAleatori.IsChecked = up.Generacio.Equals(Opcions.ALEATORI);
                    }
                    if (up.ColorNoSortit != null)
                        cpNoSortit.SelectedColor = (Color)ColorConverter.ConvertFromString(up.ColorNoSortit);
                    if (up.ColorSortit != null)
                        cpSortit.SelectedColor = (Color)ColorConverter.ConvertFromString(up.ColorSortit);
                    myFileStream.Close();
                }
                catch(Exception ex)
                {
                    
                }
            }    
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PreferenciesUsuari up = new PreferenciesUsuari();
            if (nudMaxim.Value != null)
                up.Maxim = (int)nudMaxim.Value;
            else
                up.Maxim = 100;
            if (nudMinim.Value != null)
                up.Minim = (int)nudMinim.Value;
            else
                up.Minim = 1;
            if (tbFreq.Value!=null)
                up.Frequencia =(double)tbFreq.Value;
            else
                up.Frequencia = 2;
            up.ColorNoSortit = cpNoSortit.SelectedColor.Value.ToString();
            up.ColorSortit = cpSortit.SelectedColor.Value.ToString();
            up.Pronuncia = (bool)rbtVeuGrabada.IsChecked ? Opcions.GRABADA : Opcions.TEXTTOSPEECH;
            up.Repeticions = (int)nudRepeticions.Value;
            up.Aparicio = (bool)rbtAutomatic.IsChecked ? Opcions.AUTOMATIC : Opcions.MANUAL;
            up.Generacio = (bool)rbtAleatori.IsChecked ? Opcions.ALEATORI : Opcions.DIRECTE;
            XmlSerializer mySerializer = new XmlSerializer(typeof(PreferenciesUsuari));
            StreamWriter myWriter = new StreamWriter("prefs.xml");
            mySerializer.Serialize(myWriter, up);
            myWriter.Close();
            Window parentWindow = Window.GetWindow(this);
            Principal princ = new Principal();
            ((MainWindow)parentWindow).TransContent = princ;
        }
    }
}
