﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bingo
{
    public class PreferenciesUsuari
    {
        double  frequencia;
        String colorSortit, colorNoSortit, pronuncia, aparicio, generacio;
        int repeticions, minim, maxim;

        public int Minim
        {
            get
            {
                return minim;
            }

            set
            {
                minim = value;
            }
        }

        public int Maxim
        {
            get
            {
                return maxim;
            }

            set
            {
                maxim = value;
            }
        }

        public double Frequencia
        {
            get
            {
                return frequencia;
            }

            set
            {
                frequencia = value;
            }
        }

        public string ColorSortit
        {
            get
            {
                return colorSortit;
            }

            set
            {
                colorSortit = value;
            }
        }

        public string ColorNoSortit
        {
            get
            {
                return colorNoSortit;
            }

            set
            {
                colorNoSortit = value;
            }
        }

        public string Pronuncia
        {
            get
            {
                return pronuncia;
            }

            set
            {
                pronuncia = value;
            }
        }

        public string Aparicio
        {
            get
            {
                return aparicio;
            }

            set
            {
                aparicio = value;
            }
        }

        public string Generacio
        {
            get
            {
                return generacio;
            }

            set
            {
                generacio = value;
            }
        }

        public int Repeticions
        {
            get
            {
                return repeticions;
            }

            set
            {
                repeticions = value;
            }
        }
    }
}
