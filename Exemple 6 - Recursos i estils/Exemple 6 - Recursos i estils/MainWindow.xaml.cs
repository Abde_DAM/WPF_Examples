﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exemple_6___Recursos_i_estils
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // Fer servir el dinàmic només si canvia el recurs, sino estàtic
        private void btnVermell_Click(object sender, RoutedEventArgs e)
        {
            // Fa referència a la finestra, retorna un recurs, per tant es posa SolidColorBrush
            // Obtenir recurs des de codi
            // (pinzell) apuntarà al mateix lloc que (PinzellBlau)

            // SolidColorBrush pinzell = (SolidColorBrush)this.Resources["PinzellBlau"]; SUBSTITUIR PER:
            SolidColorBrush pinzell = (SolidColorBrush)FindResource("PinzellBlau"); // Busca des del botó cap endalt
            if (!pinzell.IsFrozen)
            {
                pinzell.Color = Colors.Firebrick;
            }


        }

        private void btnVerd_Click(object sender, RoutedEventArgs e)
        {
            Object obj = TryFindResource("PinzellBlau");
            if (obj != null)
            {
                SolidColorBrush pinzell = (SolidColorBrush)obj;
                // Crearà un nou pinzell
                this.Resources["PinzellBlau"] = new SolidColorBrush(Colors.ForestGreen);
            }
        }
    }
}
