﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Menu
{
    /// <summary>
    /// Lógica de interacción para Colors.xaml
    /// </summary>
    public partial class Colors : Window
    {
        SolidColorBrush pinzellFons;

        public Colors()
        {
            InitializeComponent();
            lblA.Content = "A: " + Math.Round(sbAa.Value, 0);
            lblR.Content = "R: " + Math.Round(sbRedd.Value, 0);
            lblG.Content = "G: " + Math.Round(sbGreenn.Value, 0);
            lblB.Content = "B: " + Math.Round(sbBluee.Value, 0);


            pinzellFons = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            lblColor.Background= pinzellFons;
        }
        public SolidColorBrush Fons
        {
            get { return pinzellFons; }
        }

        private void sbA(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sbAa != null && pinzellFons != null)
                pinzellFons.Color = Color.FromArgb((byte)sbAa.Value, (byte)sbRedd.Value, (byte)sbGreenn.Value, (byte)sbBluee.Value);
            slAa.Value = sbAa.Value;
            lblA.Content = "A: " + Math.Round(sbAa.Value, 0);
        }

        private void sbRed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sbAa != null && pinzellFons != null)
                pinzellFons.Color = Color.FromArgb((byte)sbAa.Value, (byte)sbRedd.Value, (byte)sbGreenn.Value, (byte)sbBluee.Value);
            slRedd.Value = sbRedd.Value;
            lblR.Content = "R: " + Math.Round(sbRedd.Value, 0);
        }

        private void sbBlue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sbAa != null && pinzellFons != null)
                pinzellFons.Color = Color.FromArgb((byte)sbAa.Value, (byte)sbRedd.Value, (byte)sbGreenn.Value, (byte)sbBluee.Value);
            slBluee.Value = sbBluee.Value;
            lblB.Content = "B: " + Math.Round(sbBluee.Value, 0);
        }

        private void sbGreen(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sbAa != null && pinzellFons != null)
                pinzellFons.Color = Color.FromArgb((byte)sbAa.Value, (byte)sbRedd.Value, (byte)sbGreenn.Value, (byte)sbBluee.Value);
            slGreenn.Value = sbGreenn.Value;
            lblG.Content = "G: " + Math.Round(sbGreenn.Value, 0);
        }

        private void slA(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sbAa.Value = slAa.Value;
        }


        private void slRed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sbRedd.Value = slRedd.Value;
        }

        private void slGreen(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sbGreenn.Value = slGreenn.Value;
        }

        private void slBlue(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sbBluee.Value = slBluee.Value;
        }




    }
}
