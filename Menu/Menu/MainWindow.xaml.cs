﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private double sumaPreu, total = 0.0;
        private const double PREUMENU = 12;
        private const double PLATDIA = 10;
        SolidColorBrush pinzellFons;
        SolidColorBrush pinzellFont;
        RadioButton[] rbtArray = new RadioButton[9];
        //CheckBox[] chkArray=new CheckBox[4];
        string factura;
        public MainWindow()
        {
            InitializeComponent(); 

            //c.Fons
            
            rbtArray[0] = rbtMacarrons;
            rbtArray[1] = rbtCigrons;
            rbtArray[2] = rbtSopaPeix;
            rbtArray[3] = rbtCarnForn;
            rbtArray[4] = rbtPollastre;
            rbtArray[5] = rbtLasanya;
            rbtArray[6] = rbtFlam;
            rbtArray[7] = rbtGelat;
            rbtArray[8] = rbtPastis;
        }

        private void btnFactura_Click(object sender, RoutedEventArgs e)
        {
            factura = "Client: " + txtNom.Text+ "\n";
            sumaPreu = 0.0;
            if ((bool)rbtArray[0].IsChecked && (bool)rbtArray[3].IsChecked && (bool)chkCaramelss.IsChecked)
            {
                factura += "Menú del día \n"
                + rbtArray[0].Content.ToString() + ":    -\n"
                + rbtArray[3].Content.ToString() + ":    -";
                if ((bool)rbtArray[6].IsChecked) factura += "\n" + rbtArray[6].Content.ToString() + ":    -";
                else if ((bool)rbtArray[7].IsChecked) factura += "\n" + rbtArray[7].Content.ToString() + ":    -";
                else if ((bool)rbtArray[8].IsChecked) factura += "\n" + rbtArray[8].Content.ToString() + ":    -";
                if ((bool)chkCopa.IsChecked) { factura += "\n" + chkCopa.Content.ToString() + ":    " + chkCopa.Tag; sumaPreu += double.Parse((String)chkCopa.Tag); }
                if ((bool)chkPuro.IsChecked) { factura += "\n" + chkPuro.Content.ToString() + ":    " + chkPuro.Tag; sumaPreu += double.Parse((String)chkPuro.Tag); }
                if ((bool)chkTurro.IsChecked) { factura += "\n" + chkTurro.Content.ToString() + ":    " + chkTurro.Tag; sumaPreu += double.Parse((String)chkTurro.Tag); }
                if ((bool)chkCaramel.IsChecked) { factura += "\n" + chkCaramel.Content.ToString() + ":    -" ;}
                factura += "\nPreu del menú: " + PREUMENU;
                //+"\nTOTAL: "+PREUMENU+sumaPreu;
                total = PREUMENU + sumaPreu;
                total = total + (total * 21) / 100;
                factura += "\nTOTAL: " + total;

            }
            else if (!(bool)rbtArray[0].IsChecked && !(bool)rbtArray[1].IsChecked && !(bool)rbtArray[2].IsChecked && (bool)rbtArray[3].IsChecked)
            {
                factura += "Plat del dia\n" + rbtArray[3].Content.ToString() + ":    " + double.Parse((String)rbtArray[3].Tag);
                if ((bool)rbtArray[6].IsChecked) factura += "\n" + rbtArray[6].Content.ToString() + ":    -";
                else if ((bool)rbtArray[7].IsChecked) factura += "\n" + rbtArray[7].Content.ToString() + ":    -";
                else if ((bool)rbtArray[8].IsChecked) factura += "\n" + rbtArray[8].Content.ToString() + ":    -";
                if ((bool)chkCopa.IsChecked) { factura += "\n" + chkCopa.Content.ToString() + ":    " + chkCopa.Tag; sumaPreu += double.Parse((String)chkCopa.Tag); }
                if ((bool)chkPuro.IsChecked) { factura += "\n" + chkPuro.Content.ToString() + ":    " + chkPuro.Tag; sumaPreu += double.Parse((String)chkPuro.Tag); }
                if ((bool)chkTurro.IsChecked) { factura += "\n" + chkTurro.Content.ToString() + ":    " + chkTurro.Tag; sumaPreu += double.Parse((String)chkTurro.Tag); }
                if ((bool)chkCaramel.IsChecked) { factura += "\n" + chkCaramel.Content.ToString() + ":    " + chkCaramel.Tag; sumaPreu += double.Parse((String)chkCaramel.Tag); }
                total = PLATDIA + sumaPreu;
                total = total + (total * 21) / 100;
                factura += "\nTOTAL: " + total;
            }

            else if (!(bool)rbtArray[3].IsChecked && !(bool)rbtArray[4].IsChecked&& !(bool)rbtArray[5].IsChecked&& (bool)rbtArray[0].IsChecked)
            {
                factura += "Plat del dia\n" + rbtArray[0].Content.ToString() + ":    " + double.Parse((String)rbtArray[0].Tag);
                if ((bool)rbtArray[6].IsChecked) factura += "\n" + rbtArray[6].Content.ToString() + ":    -";
                else if ((bool)rbtArray[7].IsChecked) factura += "\n" + rbtArray[7].Content.ToString() + ":    -";
                else if ((bool)rbtArray[8].IsChecked) factura += "\n" + rbtArray[8].Content.ToString() + ":    -";
                if ((bool)chkCopa.IsChecked) { factura += "\n" + chkCopa.Content.ToString() + ":    " + chkCopa.Tag; sumaPreu += double.Parse((String)chkCopa.Tag); }
                if ((bool)chkPuro.IsChecked) { factura += "\n" + chkPuro.Content.ToString() + ":    " + chkPuro.Tag; sumaPreu += double.Parse((String)chkPuro.Tag); }
                if ((bool)chkTurro.IsChecked) { factura += "\n" + chkTurro.Content.ToString() + ":    " + chkTurro.Tag; sumaPreu += double.Parse((String)chkTurro.Tag); }
                if ((bool)chkCaramel.IsChecked) { factura += "\n" + chkCaramel.Content.ToString() + ":    " + chkCaramel.Tag; sumaPreu += double.Parse((String)chkCaramel.Tag); }
                total = PLATDIA + sumaPreu;
                total = total + (total * 21) / 100;
                factura += "\nTOTAL: " + total;
            }
            else
            {
                for (int i = 0; i < rbtArray.Length; i++)
                {
                    if ((bool)rbtArray[i].IsChecked)
                    {
                        factura += rbtArray[i].Content.ToString() + ":    " + rbtArray[i].Tag + "\n";
                        sumaPreu += double.Parse((String)rbtArray[i].Tag);
                    }

                }

                if ((bool)chkCopa.IsChecked) { factura += chkCopa.Content.ToString() + ":    " + chkCopa.Tag; sumaPreu += double.Parse((String)chkCopa.Tag); }
                if ((bool)chkPuro.IsChecked) { factura += "\n" + chkPuro.Content.ToString() + ":    " + chkPuro.Tag; sumaPreu += double.Parse((String)chkPuro.Tag); }
                if ((bool)chkTurro.IsChecked) { factura += "\n" + chkTurro.Content.ToString() + ":    " + chkTurro.Tag; sumaPreu += double.Parse((String)chkTurro.Tag); }
                if ((bool)chkCaramel.IsChecked) { factura += "\n" + chkCaramel.Content.ToString() + ":    " + chkCaramel.Tag; sumaPreu += double.Parse((String)chkCaramel.Tag); }
                total = sumaPreu;
                total = total + (total * 21) / 100;
                factura += "\nTOTAL: " + total + "€";

            }
            factura f = new factura(factura);
            f.ShowDialog();
        }

        private void btnReiniciar_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < rbtArray.Length; i++)
            {
                rbtArray[i].IsChecked = false;
            }
        }

        private void chkedPuro(object sender, RoutedEventArgs e)
        {
            if ((bool)chkPuro.IsChecked) chkCopa.IsChecked = true;
        }


        private void chkCopa_Unchecked(object sender, RoutedEventArgs e)
        {
            if ((bool)chkPuro.IsChecked) { chkCopa.IsChecked = false; chkPuro.IsChecked = false; }
        }

        private void btnColorFons_Click(object sender, RoutedEventArgs e)
        {
            Colors c = new Colors();
            c.ShowDialog();
            pinzellFons = c.Fons;
            gdFons.Background = pinzellFons;
            gdFonsPost.Background = pinzellFons;
            gdFonsEx.Background = pinzellFons;
            gdFonsSeg.Background = pinzellFons;
        }

        private void btnColorFont_Click(object sender, RoutedEventArgs e)
        {
            Colors c = new Colors();
            c.ShowDialog();
            pinzellFont = c.Fons;
            gbxPostres.Foreground= pinzellFont;
            gbxPrimers.Foreground = pinzellFont;
            gbxSegons.Foreground = pinzellFont;
            gbxExtres.Foreground = pinzellFont;
        }
    }
}
