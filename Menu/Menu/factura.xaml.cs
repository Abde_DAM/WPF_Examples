﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Menu
{
    /// <summary>
    /// Lógica de interacción para factura.xaml
    /// </summary>
    public partial class factura : Window
    {
        public factura(string fact)
        {
            InitializeComponent();
            txtbFactura.Text = fact;
        }

    }
}
