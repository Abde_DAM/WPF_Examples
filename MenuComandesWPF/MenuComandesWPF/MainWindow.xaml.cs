﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MenuComandesWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool modificat = false;

        public MainWindow()
        {
            InitializeComponent();
            modificat = false;
            CommandBinding enllaçCommand = new CommandBinding(NavigationCommands.DecreaseZoom);
            enllaçCommand.Executed += EnllaçCommand_Executed;
            enllaçCommand.CanExecute += EnllaçCommand_CanExecute;
            this.CommandBindings.Add(enllaçCommand);

        }

        private void EnllaçCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = txtText!=null&&txtText.FontSize <= 11 ? false : true;
        }

        private void EnllaçCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            txtText.FontSize -= 10;
            tbZoom.Text = "Zoom: " + (int)FontSize;
        }

        private void Desmarca(MenuItem arrel)
        {
            foreach (object o in arrel.Items)
            {
                if (o is MenuItem)
                {
                    MenuItem item = (MenuItem)o;
                    item.IsChecked = false;
                }
            }
        }
        private void mnuZoomPetita_Click(object sender, RoutedEventArgs e)
        {
            this.FontSize = 10;
            Desmarca((MenuItem)((MenuItem)sender).Parent);
            ((MenuItem)sender).IsChecked = true;
        }

        private void mnuZoomNormal_Click(object sender, RoutedEventArgs e)
        {
            this.FontSize = 15;
            Desmarca((MenuItem)((MenuItem)sender).Parent);
            ((MenuItem)sender).IsChecked = true;
        }

        private void mnuZoomGrossa_Click(object sender, RoutedEventArgs e)
        {
            this.FontSize = 20;
            Desmarca((MenuItem)((MenuItem)sender).Parent);
            ((MenuItem)sender).IsChecked = true;
        }

        private void mnuMida_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            Desmarca((MenuItem)sender);
            if (FontSize < 15)
                mnuZoomPetita.IsChecked = true;
            else if (FontSize == 15)
                mnuZoomNormal.IsChecked = true;
            else
                mnuZoomGrossa.IsChecked = true;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            modificat = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(modificat)
                e.Cancel = true;
        }

        private void cmdIncraseZoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            txtText.FontSize += 10;
            tbZoom.Text = "Zoom: " + (int)txtText.FontSize;
        }

        private void cmdIncraseZoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
                e.CanExecute = txtText != null && FontSize >= 50 ? false : true;
        }

        private void btnMajor_Click(object sender, RoutedEventArgs e)
        {
            txtText.SelectionStart++;
            if (txtText.SelectionLength > 0)
                txtText.SelectionLength--;
        }

        private void btnMenor_Click(object sender, RoutedEventArgs e)
        {
            txtText.SelectionStart--;
            txtText.SelectionLength++;
        }

        private void txtText_LostFocus(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
