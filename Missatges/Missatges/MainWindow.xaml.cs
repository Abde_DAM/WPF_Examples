﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Missatges
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
           /* Binding enllaç;

            enllaç = new Binding();
            enllaç.Source = sldAmplada;
            enllaç.Path = new PropertyPath("Value");
            enllaç.Mode = BindingMode.TwoWay;
            this.SetBinding(Window.WidthProperty, enllaç);

            enllaç = new Binding();
            enllaç.Source = sldAlçada;
            enllaç.Path = new PropertyPath("Value");
            enllaç.Mode = BindingMode.TwoWay;
            this.SetBinding(Window.HeightProperty, enllaç);*/

           /* Array botons = Enum.GetValues(typeof(MessageBoxButton));
            foreach (MessageBoxButton boto in botons)
            {
                cmbBotons.Items.Add(boto);
            }*/
            string[] noms = Enum.GetNames(typeof(MessageBoxButton));

            foreach (string nom in noms)
            {
                cmbBotons.Items.Add(nom);
            }
        }

        private void btnMissatge_Click(object sender, RoutedEventArgs e)
        {
            //string novaParaula;
            //if (txtParaula.Text != "")
            //{
            //    lstParaules.Items.Add(txtParaula.Text);
            //    //lstParaules.SelectedItems();
            //}

            //MessageBox.Show("Hello World!", "Títol", MessageBoxButton.YesNo);
            /*MessageBoxResult resultat = MessageBox.Show(txtMissatge.Text, txtTitol.Text, (MessageBoxButton)cmbBotons.SelectedItem, MessageBoxImage.Warning, MessageBoxResult.Cancel);*/ //
            /*MessageBoxResult resultat = MessageBox.Show(txtMissatge.Text, txtTitol.Text,(MessageBoxButton)Enum.Parse(typeof(MessageBoxButton),cmbBotons.SelectedItem.ToString()), MessageBoxImage.Warning, MessageBoxResult.Cancel);
            switch(resultat)
            {
                case MessageBoxResult.Cancel:
                    break;
            }
            this.Title = resultat.ToString();*/

            WindMissatge finestra = new WindMissatge();
            finestra.Titol = "Alerta!";
            finestra.Missatge = "Quin color t'agrada més?";
            finestra.PossiblesRespostes = new string[] { "Blanc", "Blau", "Verd", "Taronja", "Lila" };
            finestra.ShowDialog();
            this.Title=finestra.Resposta;
        }

        private void cmbBotons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void sldSeleccionat_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void sldMidaFont_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

    }
}