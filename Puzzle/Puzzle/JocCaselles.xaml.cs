﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Puzzle
{
    /// <summary>
    /// Lógica de interacción para JocCaselles.xaml
    /// </summary>
    public partial class JocCaselles : Window
    {
        int movimients = 0;
        Point buit = new Point();
        string aux;
        DispatcherTimer crono;
        TimeSpan transcorregut;
        SolidColorBrush pinzellVermell = new SolidColorBrush(Color.FromRgb(255, 0, 0));
        SolidColorBrush pinzellVerd = new SolidColorBrush(Color.FromRgb(0, 255, 0));
        SolidColorBrush pinzellBlanc = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        TextBlock[,] matriu;
        private int files, columnes;
        private const int CNFILES = 5, CNCOLUMNES = 5;
        public JocCaselles()
            : this(CNFILES, CNCOLUMNES)
        { }
        public JocCaselles(int files, int columnes)
        {
            InitializeComponent();
            this.files = files;
            this.columnes = columnes;
            Inicialitzar();
            crono = new DispatcherTimer();
            crono.Interval = TimeSpan.FromSeconds(0.1);
            crono.Tick += crono_Tick;
            crono.Start();
        }

        public void Inicialitzar()
        {
            for (int i = 0; i < columnes; i++)
            {
                gdPrincipal.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < files; i++)
            {
                gdPrincipal.RowDefinitions.Add(new RowDefinition());
            }
            matriu = new TextBlock[files, columnes];
            int valor = 0;
            buit.Y = files - 1;
            buit.X = columnes - 1;
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(1); j++)
                {
                    valor++;
                    matriu[i, j] = new TextBlock();
                    matriu[i, j].Text = valor.ToString();
                    matriu[i, j].Tag = new Point(i, j);
                    matriu[i, j].TextAlignment = TextAlignment.Center;
                    matriu[i, j].Margin = new Thickness(5);
                    matriu[i, j].SetValue(Grid.ColumnProperty, j);
                    matriu[i, j].SetValue(Grid.RowProperty, i);
                    matriu[i, j].MouseDown += TextBlock_MouseDown;

                    if (j == columnes - 1 && i == files - 1)
                    {
                        matriu[i, j].Background = pinzellBlanc;
                        matriu[i, j].Text = " ";
                    }
                    else
                        ColorRG(i, j);
                    gdPrincipal.Children.Add(matriu[i, j]);
                }
            }
            Barrejar();
            Solucionable();

        }
        private void ColorRG(int i, int j)
        {
            if (i * columnes + j + 1 == int.Parse(matriu[i, j].Text))
            {
                matriu[i, j].Background = pinzellVerd;
            }
            else
                matriu[i, j].Background = pinzellVermell;

        }
        private void Intercambiar(int i, int j)
        {
            aux = matriu[i, j].Text;
            matriu[i, j].Text = matriu[(int)buit.Y, (int)buit.X].Text;
            matriu[(int)buit.Y, (int)buit.X].Text = aux;
            matriu[i, j].Background = pinzellBlanc;
            buit.Y = i;
            buit.X = j;
        }
        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock t = (TextBlock)sender;
            string[] s = t.Tag.ToString().Split(';');
            int i = int.Parse(s[0]);
            int j = int.Parse(s[1]);
            
            if ((int)buit.X == j)
            {
                if (buit.Y > i)
                {
                    for (int k = (int)buit.Y - 1; k >= i; k--)
                    {
                        Intercambiar(k, j);
                        movimients++;
                        ColorRG((int)buit.Y + 1, (int)buit.X);
                    }
                }
                else if (buit.Y < i)
                {
                    for (int k = (int)buit.Y + 1; k <= i; k++)
                    {
                        Intercambiar(k, j);
                        movimients++;

                        ColorRG((int)buit.Y - 1, (int)buit.X);
                    }
                }
            }
            else if ((int)buit.Y == i)
            {
                if ((int)buit.X > j)
                {
                    for (int k = (int)buit.X - 1; k >= j; k--)
                    {
                        Intercambiar(i, k);
                        movimients++;
                        ColorRG((int)buit.Y, (int)buit.X + 1);
                    }

                }
                else if ((int)buit.X < j)
                {
                    for (int k = (int)buit.X + 1; k <= j; k++)
                    {
                        Intercambiar(i, k);

                        movimients++;
                        ColorRG((int)buit.Y, (int)buit.X - 1);
                    }
                }
            }
                tbMoviments.Text = " Moviments: " + movimients;
                if (Correcte())
                {
                    crono.Stop();
                    MessageBox.Show("Completat!");
                    this.Close();
                }
            
        }
        private bool Correcte()
        {
            int i = 0, j = 0;
            bool correcte = true;
            while (i < matriu.GetLength(0) && correcte)
            {
                j = 0;
                while (j < matriu.GetLength(1) && correcte)
                {
                    if (matriu[i, j] != matriu[(int)buit.Y, (int)buit.X])
                        correcte = matriu[i, j].Background == pinzellVerd;
                    j++;
                }
                i++;
            }
            return correcte;
        }
        private void Window_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            bool ctrl = false;
            if ((e.KeyboardDevice.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                ctrl = true;
            if (e.Key == Key.P)
            {
                if (gdPrincipal.Visibility != Visibility.Hidden)
                {
                    gdPrincipal.Visibility = Visibility.Hidden;
                    crono.Stop();

                }
                else
                {
                    gdPrincipal.Visibility = Visibility.Visible;
                    crono.Start();
                }
            }
            else if (e.Key == Key.Down)
            {
                if ((int)buit.Y - 1 >= 0)
                {
                    if (ctrl)
                        for (int k = (int)buit.Y - 1; k >= 0; k--)
                        {
                            Intercambiar(k, (int)buit.X);
                            movimients++;
                            ColorRG((int)buit.Y + 1, (int)buit.X);
                        }
                    else
                    {
                        matriu[(int)buit.Y - 1, (int)buit.X].Background = pinzellBlanc;
                        Intercambiar((int)buit.Y - 1, (int)buit.X);
                        ColorRG((int)buit.Y + 1, (int)buit.X);
                        movimients++;
                    }

                }
            }
            else if (e.Key == Key.Up)
            {
                if ((int)buit.Y + 1 < files)
                {
                    if (ctrl)
                        for (int k = (int)buit.Y + 1; k < files; k++)
                        {
                            Intercambiar(k, (int)buit.X);
                            movimients++;
                            ColorRG((int)buit.Y - 1, (int)buit.X);
                        }
                    else
                    {
                        matriu[(int)buit.Y + 1, (int)buit.X].Background = pinzellBlanc;
                        Intercambiar((int)buit.Y + 1, (int)buit.X);
                        ColorRG((int)buit.Y - 1, (int)buit.X);
                        movimients++;
                    }
                }
            }
            else if (e.Key == Key.Right)
            {
                if ((int)buit.X - 1 >= 0)
                {
                    if (ctrl)
                        for (int k = (int)buit.X - 1; k >= 0; k--)
                        {
                            Intercambiar((int)buit.Y, k);
                            movimients++;
                            ColorRG((int)buit.Y, (int)buit.X + 1);
                        }
                    else
                    {
                        matriu[(int)buit.Y, (int)buit.X - 1].Background = pinzellBlanc;
                        Intercambiar((int)buit.Y, (int)buit.X - 1);
                        ColorRG((int)buit.Y, (int)buit.X + 1);
                        movimients++;
                    }
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((int)buit.X + 1 < columnes)
                {
                    if (ctrl)
                    {
                        for (int k = (int)buit.X + 1; k < columnes; k++)
                        {
                            Intercambiar((int)buit.Y, k);
                            movimients++;
                            ColorRG((int)buit.Y, (int)buit.X - 1);
                        }
                    }
                    else
                    {
                        matriu[(int)buit.Y, (int)buit.X + 1].Background = pinzellBlanc;
                        Intercambiar((int)buit.Y, (int)buit.X + 1);
                        ColorRG((int)buit.Y, (int)buit.X - 1);
                        movimients++;
                    }
                }
            }


            tbMoviments.Text = " Moviments: " + movimients;
            if (Correcte())
            {
                crono.Stop();
                MessageBox.Show("Completat!");
                this.Close();
            }
        }
        private void crono_Tick(object sender, EventArgs e)
        {
            transcorregut = transcorregut.Add(crono.Interval);
            tbCrono.Text = String.Format("{0:00}:{1:00}:{2:0}", transcorregut.Minutes, transcorregut.Seconds, transcorregut.Milliseconds / 100);
        }
        private void Barrejar()
        {
            string auxiliar;
            int x1, x2, y1, y2;
            int potencia = (int)Math.Pow(files * columnes - 1, 2);
            Random r = new Random();
            for (int i = 0; i < potencia; i++)
            {
                x1 = r.Next(0, files);
                x2 = r.Next(0, files);
                y1 = r.Next(0, columnes);
                y2 = r.Next(0, columnes);
                if (matriu[x1, y1] != matriu[(int)buit.Y, (int)buit.X] && matriu[x2, y2] != matriu[(int)buit.Y, (int)buit.X])
                {
                    auxiliar = matriu[x1, y1].Text;
                    matriu[x1, y1].Text = matriu[x2, y2].Text;
                    matriu[x2, y2].Text = auxiliar;
                    ColorRG(x1, y1);
                    ColorRG(x2, y2);
                }
            }

        }
        private void Solucionable()
        {
            int[] llistaValors = new int[files * columnes - 1];
            int comptador = 0, k = 0;
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(1); j++)
                {
                    if (matriu[i, j] != matriu[(int)buit.Y, (int)buit.X])
                        llistaValors[k] = int.Parse(matriu[i, j].Text);
                    k++;
                }
            }
            for (int i = 0; i < llistaValors.Length; i++)
            {
                for (int j = i; j < llistaValors.Length; j++)
                {
                    if (llistaValors[i] > llistaValors[j])
                        comptador++;
                }
            }

            if (comptador == 0 || comptador % 2 == 1)
            {
                aux = matriu[files - 1, columnes - 2].Text;
                matriu[files - 1, columnes - 2].Text = matriu[files - 1, columnes - 3].Text;
                matriu[files - 1, columnes - 3].Text = aux;
                ColorRG(files - 1, columnes - 2);
                ColorRG(files - 1, columnes - 3);
            }

        }
    }
}
