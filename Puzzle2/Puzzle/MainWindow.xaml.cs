﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Puzzle
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            JocCaselles nom = new JocCaselles(Convert.ToInt32(sldFiles.Value), Convert.ToInt32(sldColumnes.Value));
            nom.ShowDialog();
        }

        private void sldFiles_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbFiles.Content="Files: "+((Slider)sender).Value;
        }

        private void sldColumnes_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbColumnes.Content = "Columnes: " + ((Slider)sender).Value;
        }
    }
}
