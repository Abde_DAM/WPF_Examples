﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RecursosEstils
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnVermell_Click(object sender, RoutedEventArgs e)
        {
            //SolidColorBrush pinzell=(SolidColorBrush)this.Resources["PinzellBlau"];
            SolidColorBrush pinzell = (SolidColorBrush)FindResource("PinzellBlau");
            if(!pinzell.IsFrozen)
            pinzell.Color = Colors.Firebrick;
        }

        private void btnVerd_Click(object sender, RoutedEventArgs e)
        {
            // Crearà un nou pinzell
            Object obj =TryFindResource("PinzellBlau");
            if (obj != null)
            {
                SolidColorBrush pinzell = (SolidColorBrush)obj;
                this.Resources["PinzellBlau"] = new SolidColorBrush(Colors.ForestGreen);
            }

        }
    }
}
