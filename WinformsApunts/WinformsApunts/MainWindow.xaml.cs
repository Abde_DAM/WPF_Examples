﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WinformsApunts
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SolidColorBrush pinzellFons;
        SolidColorBrush pinzellPanell;
        public MainWindow()
        {
            InitializeComponent();
            pinzellFons = new SolidColorBrush(Color.FromRgb(127, 127, 127));
            pinzellPanell = new SolidColorBrush(Color.FromArgb(127, 0, 0, 255));
            this.Background = pinzellFons;
            spFons.Background = pinzellPanell;
        }

        private void btnProva_Click(object sender, RoutedEventArgs e)
        {
            Nova nova = new Nova();
            nova.ShowDialog();
            this.Title = nova.Nom;
           /* Button nouBoto;
            pinzellFons.Color = (Color.FromRgb(255, 0, 0));

           for (int i = 0; i < 10; i++)
            {
                nouBoto = new Button();
                nouBoto.Content = "Boto nº " + (i + 1);
                spBotons.Children.Add(nouBoto);
            }*/
            
        }

        private void sbTransparencia_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //this.Title = sbTransparencia.Value.ToString();
            if(sbTransparencia!=null&&pinzellFons!=null&&pinzellPanell!=null)
                pinzellPanell.Color = Color.FromArgb((byte)sbTransparencia.Value, (byte)slVermell.Value, 0, 255);
        }

        private void slVermell_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (slVermell != null && pinzellFons != null && pinzellPanell != null)
                pinzellPanell.Color = Color.FromArgb((byte)sbTransparencia.Value, (byte)slVermell.Value, 0, 255);
        }

        
    }
}
