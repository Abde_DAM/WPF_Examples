﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WinformsApunts
{
    /// <summary>
    /// Lógica de interacción para Nova.xaml
    /// </summary>
    public partial class Nova : Window
    {
        String nom;
        public String Nom
        {
            get { return nom; }
            set { nom = value; }

        }
        public Nova()
        {
            InitializeComponent();
        }

        private void txtNom_TextChanged(object sender, TextChangedEventArgs e)
        {
            nom = txtNom.Text;
        }
    }
}
